package handler

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/testdata/protoexample"
	"net/http"
)

func SomeXML(c *gin.Context) {
	c.XML(http.StatusOK, gin.H{
		"message": "hey",
		"status":  http.StatusOK,
	})
}

func SomeYaml(c *gin.Context) {
	c.YAML(http.StatusOK, gin.H{
		"message": "hey",
		"status":  http.StatusOK,
	})
}

func MoreJson(c *gin.Context) {
	var msg struct {
		Name    string `json:"user"`
		Message string
		Number  int
	}

	msg.Number = 123
	msg.Message = "hey"
	msg.Name = "Lena"

	c.JSON(http.StatusOK, msg)
}

func Protobuf(c *gin.Context) {
	reps := []int64{int64(1), int64(2)}
	label := "test"
	data := &protoexample.Test{
		Label: &label,
		Reps:  reps,
	}

	c.ProtoBuf(http.StatusOK, data)
}
