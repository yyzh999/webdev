package main

import (
	"github.com/gin-gonic/gin"
	"html/template"
)

func InitTemplate(router *gin.Engine) {
	/**
	router.Delims 和 router.SetFuncMap 要放到router.LoadHTMLFiles 的前面, 因为在LoadHTMLFiles时 设置delims和funcMap
	*/
	router.Delims("{[{", "}]}")
	router.SetFuncMap(template.FuncMap{
		"formatAsDate": formatAsDate,
	})

	router.Static("/static/assets", "./static/assets")

	router.LoadHTMLFiles("static/template/index.tmpl",
		"static/template/posts/index.tmpl",
		"static/template/raw.tmpl",
		"static/template/users/index.tmpl",
		"static/template/https.tmpl",
		"static/form.tmpl",
	)

}
