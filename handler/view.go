package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func Index(c *gin.Context) {
	c.HTML(http.StatusOK, "index.tmpl", gin.H{
		"title": "Main website",
	})
}

func PostIndex(c *gin.Context) {
	c.HTML(http.StatusOK, "posts/index.tmpl", gin.H{
		"title": "Posts",
	})
}

func UserIndex(c *gin.Context) {
	c.HTML(http.StatusOK, "users/index.tmpl", gin.H{
		"title": "User",
	})
}

func FormatAsDate(t time.Time) string {
	year, month, day := t.Date()
	return fmt.Sprintf("%d/%02d/%02d", year, month, day)
}

func Raw(c *gin.Context) {
	c.HTML(http.StatusOK, "raw.tmpl", map[string]interface{}{
		"now": time.Date(2017, 07, 01, 0, 0, 0, 0, time.UTC),
	})
}

func FormHtml(c *gin.Context) {
	c.HTML(http.StatusOK, "form.tmpl", gin.H{})
}
