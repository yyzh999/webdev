package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func SomeDataFromReader(c *gin.Context) {
	response, err := http.Get("https://www.keaidian.com/uploads/allimg/190424/24110307_8.jpg")
	if err != nil || response.StatusCode != http.StatusOK {
		c.Status(http.StatusServiceUnavailable)
		return
	}

	reader := response.Body
	contentLength := response.ContentLength
	contentType := response.Header.Get("Content-Type")

	extraHeader := map[string]string{
		"Content-Disposition": `attachment;filename="gopher.png"`,
	}

	c.DataFromReader(http.StatusOK, contentLength, contentType, reader, extraHeader)
}
