package handler

import (
	"github.com/gin-gonic/gin"
	"log"
	"time"
)

func LongAsync(c *gin.Context) {
	//创建 在goroutine中使用的副本
	cCp := c.Copy()
	go func() {
		// 用 time.Sleep() 模拟一个长任务
		time.Sleep(10 * time.Second)

		//请注意您使用的复制的上下文"cCp",这一点很重要
		log.Println("Done! in path " + cCp.Request.URL.Path)
	}()
}

func LongSync(c *gin.Context) {
	//用 time.Sleep() 模拟一个长任务
	time.Sleep(10 * time.Second)

	log.Println("Done! in path " + c.Request.URL.Path)
}
