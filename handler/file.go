package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func UploadSingleFile(c *gin.Context) {
	file, _ := c.FormFile("file")
	log.Println(file.Filename)

	c.SaveUploadedFile(file, "./static/file/"+file.Filename)
	c.String(http.StatusOK, fmt.Sprintf("'%s' uploaded!", file.Filename))
}

func UploadMultiFile(c *gin.Context) {
	form, _ := c.MultipartForm()
	files := form.File["upload"]

	for _, file := range files {
		log.Println(file.Filename)
		c.SaveUploadedFile(file, "./static/file/"+file.Filename)
	}

	c.String(http.StatusOK, fmt.Sprintf("%d files uploaded!", len(files)))
}
