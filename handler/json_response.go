package handler

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func AsciiJSON(c *gin.Context) {
	data := map[string]interface{}{
		"lang": "GO语言",
		"tag":  "<br>",
	}
	c.AsciiJSON(http.StatusOK, data)
}

func JSONP(c *gin.Context) {
	data := map[string]interface{}{
		"lang": "GO语言",
		"tag":  "<br>",
	}
	c.JSONP(http.StatusOK, data)
}

func NormalJson(c *gin.Context) {
	data := map[string]interface{}{
		"lang": "GO语言",
		"tag":  "<br>",
	}
	c.JSON(http.StatusOK, data)
}

//带缩进的JSON，pretty 格式

func IndentedJSON(c *gin.Context) {
	data := map[string]interface{}{
		"lang": "GO语言",
		"tag":  "<br>",
	}
	c.IndentedJSON(http.StatusOK, data)
}

func SecureJSON(c *gin.Context) {
	//data := map[string]interface{}{
	//	"lang":  "GO语言",
	//	"tag":   "<br>",
	//	"array": []string{"数组", "aaa", "xxx", "\n", "</br>"},
	//}
	data := []string{"数组", "aaa", "xxx", "\n", "</br>"}
	c.SecureJSON(http.StatusOK, data)
}

//PureJSON 原始JSON，不做转移

func PureJSON(c *gin.Context) {
	data := map[string]interface{}{
		"lang": "GO语言",
		"tag":  "<br>",
	}
	c.PureJSON(http.StatusOK, data)
}
